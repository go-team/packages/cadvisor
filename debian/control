Source: cadvisor
Section: admin
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Shengjing Zhu <zhsj@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-blang-semver-dev,
               golang-github-containerd-containerd-dev,
               golang-github-containerd-typeurl-dev,
               golang-github-docker-go-connections-dev,
               golang-github-docker-go-units-dev,
               golang-github-euank-go-kmsg-parser-dev,
               golang-github-garyburd-redigo-dev,
               golang-github-go-logr-logr-dev,
               golang-github-gogo-protobuf-dev,
               golang-github-karrick-godirwalk-dev,
               golang-github-kr-pretty-dev,
               golang-github-mesos-mesos-go-dev,
               golang-github-opencontainers-runc-dev (>= 1.0.0~rc93),
               golang-github-opencontainers-specs-dev,
               golang-github-pkg-errors-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev,
               golang-github-rican7-retry-dev,
               golang-github-shopify-sarama-dev,
               golang-github-stretchr-testify-dev,
               golang-go-zfs-dev,
               golang-golang-x-net-dev,
               golang-golang-x-oauth2-dev,
               golang-golang-x-sys-dev,
               golang-google-api-dev,
               golang-google-grpc-dev,
               golang-gopkg-olivere-elastic.v2-dev,
Standards-Version: 4.5.1
Homepage: https://github.com/google/cadvisor
Vcs-Browser: https://salsa.debian.org/go-team/packages/cadvisor
Vcs-Git: https://salsa.debian.org/go-team/packages/cadvisor.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/google/cadvisor

Package: golang-github-google-cadvisor-dev
Section: golang
Architecture: all
Depends: golang-github-blang-semver-dev,
         golang-github-containerd-containerd-dev,
         golang-github-docker-go-connections-dev,
         golang-github-docker-go-units-dev,
         golang-github-euank-go-kmsg-parser-dev,
         golang-github-go-logr-logr-dev,
         golang-github-gogo-protobuf-dev,
         golang-github-karrick-godirwalk-dev,
         golang-github-opencontainers-runc-dev (>= 1.0.0~rc93),
         golang-github-opencontainers-specs-dev,
         golang-github-pkg-errors-dev,
         golang-github-prometheus-client-golang-dev,
         golang-github-prometheus-client-model-dev,
         golang-github-prometheus-common-dev,
         golang-go-zfs-dev,
         golang-golang-x-net-dev,
         golang-golang-x-sys-dev,
         golang-google-grpc-dev,
         ${misc:Depends},
Description: analyze resource usage and performance of running containers
 cAdvisor (Container Advisor) provides container users an understanding of
 the resource usage and performance characteristics of their running
 containers.
 .
 cAdvisor has native support for Docker containers and should support just
 about any other container type out of the box.
 .
 cAdvisor also exposes container stats as Prometheus (http://prometheus.io)
 metrics.
 .
 This package provides golang library sources.

Package: cadvisor
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: analyze resource usage and performance characteristics of running containers
 cAdvisor (Container Advisor) provides container users an understanding of
 the resource usage and performance characteristics of their running
 containers.
 .
 cAdvisor has native support for Docker containers and should support just
 about any other container type out of the box.
 .
 cAdvisor also exposes container stats as Prometheus (http://prometheus.io)
 metrics.
 .
 This package provides daemon that collects, aggregates, processes, and
 exports information about running containers. Specifically, for each
 container it keeps resource isolation parameters, historical resource
 usage, histograms of complete historical resource usage and network
 statistics. This data is exported by container and machine-wide.
